﻿CREATE PROCEDURE spGetEmployees
  AS 
  BEGIN
 SELECT [Id]
      ,[FirstName]
      ,[LastName]
      ,[CompanyId]
      ,[Phone]
      ,[Email]
      ,[Address]
  FROM [Users]
  END