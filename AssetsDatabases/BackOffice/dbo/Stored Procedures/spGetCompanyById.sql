﻿CREATE PROCEDURE [dbo].[spGetCompanyById]
@CompanyId int
AS
BEGIN
SELECT c.[Id],
       [RegirstartionNo]
      ,c.[Name]
	  ,[PackageId]
      ,[Address]
      ,[Phone]
      ,[Email]
      ,[YearFound]
      ,[DateCreated]
FROM  [dbo].[Company] c INNER JOIN [Package] p
       On c.[PackageId] = p.Id
  WHERE c.[Id]=@CompanyId
END