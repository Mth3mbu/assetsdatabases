﻿CREATE TABLE Package
(
[Id]  INT PRIMARY KEY IDENTITY(1,1),
[Package] VARCHAR (50),
[Description] VARCHAR (250)
)
