﻿CREATE PROCEDURE spInsertEmployees
@FirstName VARCHAR(50),
@LastName VARCHAR(50),
@CompanyId INT,
@Phone VARCHAR(10),
@Email VARCHAR(50),
@Address VARCHAR(150),
@Password VARCHAR(20)
AS
BEGIN
 INSERT INTO [USers]
      ([FirstName]
      ,[LastName]
      ,[CompanyId]
      ,[Phone]
      ,[Email]
      ,[Address]
      ,[Password])
	  VALUES
	  (
	  @FirstName,
	  @LastName,
	  @CompanyId,
	  @Phone,
	  @Email ,
	  @Address,
	  @Password
	  )
  END
