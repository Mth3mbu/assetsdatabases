﻿CREATE PROCEDURE spFilterCompaniesByName
@filterString VARCHAR(4)
AS
BEGIN
SELECT [Id],
       [RegirstartionNo]
      ,[Name]
      ,[Address]
      ,[Phone]
      ,[Email]
      ,[YearFound]
      ,[DateCreated]
FROM  [dbo].[Company]
WHERE [Name] LIKE  CONCAT('%',@filterString,'%')
END
