﻿CREATE PROCEDURE spInsertCompany
 @RegirstartionNo varchar(20),
 @Name VARCHAR(100),
 @Address VARCHAR(150),
 @Phone VARCHAR(10),
 @Email VARCHAR(50),
 @YearFound  VARCHAR(4)
AS
BEGIN
INSERT INTO [dbo].[Company]
           ([RegirstartionNo]
           ,[Name]
           ,[Address]
           ,[Phone]
           ,[Email]
           ,[YearFound]
           ,[DateCreated]
           ,[isDeleted])
     VALUES
           (@RegirstartionNo
           , @Name
           ,@Address
           ,@Phone
           ,@Email 
           ,@YearFound
           ,GETDATE()
           ,0)
END
