﻿CREATE TABLE Users(
[Id] INT,
[FirstName] VARCHAR(50),
[LastName] VARCHAR(50),
[CompanyId] INT,
[Phone] VARCHAR(10),
[Email] VARCHAR(50),
[Address] VARCHAR (150),
[Password] VARCHAR (20)
)